### Initial logger

```go
package main 

import (
    logger "bitbucket.org/gomatrix/customlogger"
)

func init() {
    logConfig := logger.Configuration{
		EnableConsole:     true,
		ConsoleLevel:      logger.Debug,
		ConsoleJSONFormat: false,
		EnableFile:        true,
		FileLevel:         logger.Warn,
		FileJSONFormat:    true,
		FileLocation:      "logging.log",
	}
	if err := logger.NewLogger(logConfig, logger.InstanceZapLogger); err != nil {
		log.Fatalf("Could not instantiate log %s", err.Error())
    }
}

func main() {
    // This your code
}
```